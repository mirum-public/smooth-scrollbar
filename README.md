
# Smooth Scrollbar
Forked from https://github.com/idiotWu/smooth-scrollbar

## Custom Build
```npm run build```
```npm run compile```
```npm run bundle```
    
## Linking to project
use
"smooth-scrollbar": "https://gitlab.com/mirum-public/smooth-scrollbar.git",
instead of 
"smooth-scrollbar": "^xx.xx",

## README
https://github.com/idiotWu/smooth-scrollbar#readme
